FROM openjdk:8u121-jdk 

MAINTAINER Luis Rodriguez Fernandez <luis.rodriguez.fernandez@cern.ch>

ADD target/products-core.jar products-core.jar

RUN chown -R 1001:0 products-core.jar

USER 1001

EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar products-core.jar" ]
