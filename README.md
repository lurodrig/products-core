## What?

This projects wants to be an **example** on how you can easily create, ship and run a simple [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)  application ( [microservice](https://martinfowler.com/articles/microservices.html)?) using [spring-boot.](https://projects.spring.io/spring-boot/) and [Docker](https://www.docker.com/). I will also show you how you can run your integration test and build it in our [gitlab-ci](https://gitlab.cern.ch/lurodrig/products-core/pipelines). 

The application expose a simple [REST API](https://spring.io/understanding/REST) that performs Create, Read, Update and Delete operations against a table in a database. 

If you are looking for a platform to deploy your application I would recommend you to have a look to the [PaaS Web Application service](https://cern.service-now.com/service-portal/article.do?n=KB0004358) ([openshift](https://docs.openshift.org/latest/welcome/index.html)).

## Why?

Since 2012 I have been the developer and service manager of the [CERN Java Web Hosting Service](https://information-technology.web.cern.ch/services/java-web-hosting) and during this years I've seen that the number of applications that uses **spring-boot** has considerably increased, so this project can be seen as a small *cheat-sheet* for this technology. 
 
But my main motivation is that the **CERN Java Web Hosting Service** is going to be **substituted** by the **PaaS Web Application** one. So this application wants to be a simple, but fully working, example for helping other developers in this migration. You can find more information about this on this [KB article](https://cern.service-now.com/service-portal/article.do?n=KB0004488).   

## How?

The web services endpoints are managed by the [RestControllers](http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/bind/annotation/RestController.html). The logic is pretty simple:

    @RestController
    public class ProductController {
    
    	@Autowired
    	private ProductServices productServices;
    
    	@GetMapping("/products")
    	public List<Product> findAll() {
    		return productServices.findAll();
    	}
    }

The above snippet creates a web service that retrieves all the products of the database.  The [@Autowired](http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/beans/factory/annotation/Autowired.html) annotation tells spring to [inject](http://www.jamesshore.com/Blog/Dependency-Injection-Demystified.html)  the ProductServices. This ProductServices encapsulates the business logic of the application. 

For the database mapping I will rely on the good and classic [Java Persistence API](https://en.wikipedia.org/wiki/Java_Persistence_API). All that I need is to define my [POJO class](https://spring.io/understanding/POJO)

    @Entity
    public class Product {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.AUTO)
    	private int id;`enter code here`
    	@NotNull
    	@Pattern(regexp="^[a-zA-Z0-9_\\-]*$")
    	@Size(max=50)
    	private String name;
    	@Pattern(regexp="^[a-zA-Z0-9_\\-@\\s]*$")
    	@Size(max=50)
    	private String description;
    	@NotNull
    	private Date arrival;
    	@Min(1)
    	private int quantity;
    	
    	public Product(){}
    }

All the magic is done by the [@Entity](https://docs.oracle.com/javaee/7/api/index.html?javax/persistence/package-summary.html) annotation. It will tell JPA that your class is going to be mapped against a table in a database and persisted.

The CRUD database operations are implemented using a [CrudRepository](http://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html). As it is stated in the [documentation](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories) the main idea of using spring repositories is to reduce the amount of boiler-plate code of your data access layer.

    public interface ProductRepository extends CrudRepository<Product, Integer> {
    
    	public List<Product> findByName(String name);
    	public List<Product> findByNameStartingWith (String term);
    	public List<Product> findByNameLike (String term);
    	public List<Product> findByNameContaining (String term);
    }

The database connection is configured using a datasource. By default spring will pick the **tomcat** one. If you are using the **spring-boot-starter-data-jpa** like in this application the **tomcat-jdbc** dependency will be added automatically to your project. Keep this in mind your datasource configuration will look like this:

    # Spring datasource configuration
    spring.datasource.driver-class-name=${PRODUCTS_DB_DRIVER}
    spring.datasource.url=${PRODUCTS_DB_URL}
    spring.datasource.username=${PRODUCTS_DB_USERNAME}
    spring.datasource.password=${PRODUCTS_DB_PASSWORD}

OK we are almost done! However we still need to tell spring which database vendor we are using:

    # JPA configuration
    spring.jpa.database-platform=${PRODUCTS_JPA_DB_PLATFORM}

All this settings are configured in the [src/main/resources/application.properties](https://gitlab.cern.ch/lurodrig/products-core/blob/master/src/main/resources/application.properties)
  
## Installation

### Package

For **packaging** the application (**target/products-core.jar**):

    git clone https://:@gitlab.cern.ch:8443/lurodrig/products-core.git products-core
    cd products-core
    mvn package -DskipTests=true

Uh oh! Why I am skipping the tests? If you want to successfully run the tests (unit & integration) you **must** set the next environment variables:

    MAVEN_ORACLE_COM_USERNAME=XXXXXX
    MAVEN_ORACLE_COM_PASSWORD=XXXXXX
    PRODUCTS_JPA_DB_PLATFORM=org.hibernate.dialect.YourDBdialect
    PRODUCTS_JPA_GENERATE_DDL=true
    PRODUCTS_JPA_DDL_AUTO=validate
    PRODUCTS_DB_DRIVER=your.vendor.class.Driver
    PRODUCTS_DB_URL=jdbc:mysql://your.db.host:your.db.port/products
    PRODUCTS_DB_USERNAME=XXXXXX
    PRODUCTS_DB_PASSWORD=XXXXXX
    PRODUCTS_DB_VALIDATION_QUERY=SELECT SYSDATE FROM DUAL
    PRODUCTS_JPA_SHOW_SQL=false
    PRODUCTS_LOGGING_SPRING_FW_WEB=ERROR
    PRODUCTS_LOGGING_HIBERNATE=ERROR
    PRODUCTS_LOGGING_CH_CERN=DEBUG
    USER_SEARCH_BASE=OU=Users,OU=Organic Units,DC=your,DC=domain
    USER_DN_PATTERNS=CN={0},OU=Users,OU=Organic Units,DC=your,DC=domain
    GROUP_SEARCH_BASE=OU=Workgroups,DC=your,DC=domain
    GROUP_SEARCH_FILTER=member={0}
    GROUP_ROLE_ATTRIBUTE=cn
    MANAGER_DN=CN=YOUR_CN,OU=Users,OU=Organic Units,DC=your,DC=domain
    MANAGER_PASSWORD=XXXXX
    LDAP_AUTHENTICATOR_URL=ldaps://your.ldap.host:your.ldap.port
    ACCESS=hasRole('IT-DEP')
    PRODUCTS_LOGGING_SPRING_SEC=DEBUG
    PRODUCTS_LOGGING_HIBERNATE_TYPE=ERROR
    PRODUCTS_LOGGING_SPRING_SEC=ERROR
    PRODUCTS_JPA_HIBERNATE_FORMAT_SQL=false
    PRODUCTS_JPA_HIBERNATE_TYPE=ERROR 

Most of the above refer to database configuration and  the level of logs. However there are a few that merit further explanation.

#### Oracle Maven

Few years ago Oracle published its [maven repository](http://maven.oracle.com) (hooray!) however it is protected and you need to [register](http://www.oracle.com/webapps/maven/register/license.html) (ouch!). The `MAVEN_ORACLE_COM_USERNAME` and `MAVEN_ORACLE_COM_PASSWORD` variables store your oracle credentials. You have a fantastic blog entry about how this topic [here](https://blogs.oracle.com/dev2dev/entry/oracle_maven_repository_instructions_for).
Another option can be to directly download the driver and set it up in your maven repository. See this [entry](https://www.mkyong.com/maven/how-to-add-oracle-jdbc-driver-in-your-maven-local-repository/) at mykong.com (thanks!).

#### Spring security. LDAP configuration

Probably you want to protect your API. [spring-security](https://projects.spring.io/spring-security/) provides you a smooth, clean and simple way of doing it. The below set of environment variables configure the LDAP settings:

    USER_SEARCH_BASE=OU=Users,OU=Organic Units,DC=your,DC=domain
    USER_DN_PATTERNS=CN={0},OU=Users,OU=Organic Units,DC=your,DC=domain
    GROUP_SEARCH_BASE=OU=Workgroups,DC=your,DC=domain
    GROUP_SEARCH_FILTER=member={0}
    GROUP_ROLE_ATTRIBUTE=cn
    MANAGER_DN=CN=YOUR_CN,OU=Users,OU=Organic Units,DC=your,DC=domain
    MANAGER_PASSWORD=XXXXX
    LDAP_AUTHENTICATOR_URL=ldaps://your.ldap.host:your.ldap.port
    ACCESS=hasRole('IT-DEP')
This configuration is managed in the [WebSecurityConfig](https://gitlab.cern.ch/lurodrig/products-core/blob/master/src/main/java/ch/cern/it/security/WebSecurityConfig.java) class.

### Running 
There are many different ways of **running** the application:

 1. From your favorite IDE. E.g. in [eclipse](https://eclipse.org/) navigate to [ProductsCoreApplication](https://gitlab.cern.ch/lurodrig/products-core/blob/master/src/main/java/ch/cern/it/ProductsCoreApplication.java) and just **Run As --> Java Application**. 
 2. From the command line (`java -jar products-core.jar`)
 3. Maven with the [spring-boot maven plugin](http://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) (`mvn spring-boot:run`)
 4. Docker (`docker run -t...`). See the Running on Docker section. 
 
**Note**: if you just want to have a look at the application itself you can always pull the image from the [gitlab.cern.ch registry](https://gitlab.cern.ch/lurodrig/products-core/container_registry)...

    docker pull gitlab-registry.cern.ch/lurodrig/products-core:runner

... and just run it (see below).
### Running on Docker

In the root folder of the application you will find very simple [Dockerfile](https://gitlab.cern.ch/lurodrig/products-core/blob/master/Dockerfile):

    FROM openjdk:8u121-jdk 
    MAINTAINER Luis <luis.rodriguez.fernandez@cern.ch>
    ADD target/products-core.jar products-core.jar
    RUN chown -R 1001:0 products-core.jar
    USER 1001
    EXPOSE 8080
    ENTRYPOINT [ "sh", "-c", "java -jar products-core.jar" ]

Above code copy your packaged application (**target/products-core.jar**) to the container, run it as a random user (*1001*) and exposes it in the port 8080.  The base image **openjdk:8u121-jdk** is taken from the official [docker repository](https://hub.docker.com/_/openjdk/).

For building the container:

    docker build -t gitlab-registry.cern.ch/lurodrig/products-core:runner .

And finally for running it:

    docker run --net=host -h 127.0.0.1 --env-file .environment -p 127.0.0.1:8080:8080 -t gitlab-registry.cern.ch/lurodrig/products-core:runner

Few words about the parameters of this [run](https://docs.docker.com/engine/reference/run/) command:

 - **--net=host** tells docker to  share the host’s network stack and all interfaces from the host will be available to the container.
 - **-h 127.0.0.1**sets 127.0.0.1 as docker hostname. These two options are very convenient for development environments (e.g. you are running a MySQL database locally)
 - **--env-file .environment**: read environment variables from the .environment file.
 - **-p 127.0.0.1:8080:8080**: publish the docker 8080 port. 
 - **-t gitlab-registry.cern.ch/lurodrig/products-core:runner**: allocate a terminal for the container.

## API Reference

For documenting the REST API I have used [swagger](http://swagger.io/). Once you have the application up and running in your localhost you can have a look at http://localhost:8080/swagger-ui.html You can have a look on how to setup and configure it [here](http://www.baeldung.com/swagger-2-documentation-for-spring-rest-api).

## Tests

In the [src/test/java](https://gitlab.cern.ch/lurodrig/products-core/tree/master/src/test/java/ch/cern/it/cs/ct) you will find two kind of tests, unit and [integration](https://antoniogoncalves.org/2012/12/13/lets-turn-integration-tests-with-maven-to-a-first-class-citizen/) ones.
### Unit tests
These tests focus in specific components of your system. E.g. in our application we want to test the services offered of our [ProductServices API](https://gitlab.cern.ch/lurodrig/products-core/blob/master/src/main/java/ch/cern/it/service/ProductServices.java) This component encapsulates the business logic of our system. In our tests we want to isolate this logic from other components of the system, like the database. 

    @RunWith(SpringRunner.class)
    @SpringBootTest
    @FixMethodOrder(MethodSorters.NAME_ASCENDING)
    public class ProductServicesTests {
    
    @Autowired
    private ProductServices services;
    @MockBean
    private ProductRepository repository;
    
    @Test
    public void testA_insertWhenProductIsOK() {
    	Product product = new Product("name-name", "description@-", new Date(), 100);
    	given(repository.save(product)).willReturn(product);
    	Product result = services.saveOrUpdate(product);
    	assertThat(result).isEqualToComparingOnlyGivenFields(product, "name", "description", "arrival", "quantity");
     }
    }
In above example we are mocking (**simulating**) the behaviour of our **ProductRepository**, assuming that for a given entry the **save** method is going to produce certain result. There are many mock frameworks out there. In a spring-boot project you have a lot of chances of using [spring-boot-starter-test](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html). This dependency includes [mockito](http://site.mockito.org/) out-of-the-box. 

There are many ways of running your tests. In maven, unless you skip them they will run automatically before the **package** goal.
### Integration tests
In the other hand in these ones you want to test full functionalities of your system. E.g. in our application we want to see that the web services exposed by our **RESTController** work as expected.

    @RunWith(SpringRunner.class)
    @SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
    @FixMethodOrder(MethodSorters.NAME_ASCENDING)
    public class ProductServicesIntegrationTests {
    
    	@Autowired
    	private TestRestTemplate testRestTemplate;
    	@Autowired
    	private JsonHelper jsonHelper;
    
    	@Test
    	public void testA_getAllProducts() {
    		ResponseEntity<Product[]> response = this.testRestTemplate.getForEntity("/products", Product[].class);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody().length).isGreaterThanOrEqualTo(0);
    	}
    }
In this example we are verifying that a http request to the **"/products" service** returns the expected result.

## Continuous Integration
I use gitlab-ci for this purpose. It allows me to run my tests, build the docker image and deploy it on the CERN PaaS Web Application Service. You can have a look at the **.gitlab-ci.yml** [here](https://gitlab.cern.ch/lurodrig/products-core/blob/master/.gitlab-ci.yml). All the environment variables are injected at building time from your gitlab environment variables. You can define them in the **Settings --> Variables ** of your project.   

## Caveats

### CORS

You may wonder why in the ProductsCoreApplication we have this function:

    @Bean
    	public FilterRegistrationBean corsFilter() {
    		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    		CorsConfiguration config = new CorsConfiguration();
    		config.setAllowCredentials(true);
    		config.addAllowedOrigin("*");
    		config.addAllowedHeader("*");
    		config.addAllowedMethod("*");
    		source.registerCorsConfiguration("/**", config);
    		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
    		bean.setOrder(0);
    		return bean;
    	}

The reason is that if you want to consume your API from an user interface application, like this [one](https://gitlab.cern.ch/lurodrig/products-ui), you have to explicitly allow your application to serve requests coming from that origin. The above configuration is quite permissive and should be only used for development purposes. You can have a look at this [article](https://spring.io/understanding/cors) if you want to learn more about CORS.

### CI building. ojdbc driver from maven.oracle.com

In the installation section you have learnt how to configure your maven installation for getting the ojdbc driver from the Oracle maven official repository. If you want to do the same in your CI tool you have to provide the repository configuration (url, credentials...). That's why you can find a custom [settings.xml](https://gitlab.cern.ch/lurodrig/products-core/blob/master/settings.xml) in the root folder of the project. 

## Contributors

For any comments, questions or suggestions, please do not hesitate in contact [me](http://profiles.web.cern.ch/720335).

## Reference

Some useful tutorials

 - https://projects.spring.io/spring-boot/
 - https://spring.io/guides/gs/rest-service/
 - https://spring.io/guides/gs/accessing-data-jpa/
 - https://spring.io/guides/gs/securing-web/
 - https://springframework.guru/spring-boot-web-application-part-3-spring-data-jpa/
 - https://springframework.guru/using-h2-and-oracle-with-spring-boot/
 - https://docs.docker.com/ https://gitlab.cern.ch/help
