package ch.cern.it.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ch.cern.it.utils.ValidationPatterns;

@Entity
public class Product {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotNull
	@Pattern(regexp=ValidationPatterns.NAME)
	@Size(max=50)
	private String name;
	
	@Pattern(regexp=ValidationPatterns.DESCRIPTION)
	@Size(max=500)
	private String description;
	
	@NotNull
	private Date arrival;
	
	@Min(1)
	private int quantity;
	
	public Product(){}
	
	public Product(String name, String description, Date arrival, int quantity) {
		super();
		this.name = name;
		this.description = description;
		this.arrival = arrival;
		this.quantity = quantity;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getArrival() {
		return arrival;
	}
	public void setArrival(Date arrival) {
		this.arrival = arrival;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", description=" + description + ", arrival=" + arrival
				+ ", quantity=" + quantity + "]";
	}
}
