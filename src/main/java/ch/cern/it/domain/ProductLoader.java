package ch.cern.it.domain;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import ch.cern.it.utils.Dates;

/**
 * 
 * @author lurodrig
 *
 */
@Component
public class ProductLoader implements ApplicationListener<ContextRefreshedEvent> {

	private Logger log = Logger.getLogger(ProductLoader.class);
	private ProductRepository productRepository;
	private String[] nameOfProducts = { "sticker", 
			"tshirt", 
			"poster", 
			"mug", 
			"pen", 
			"pencil", 
			"magazine", 
			"cap",
			"polo", 
			"usb-stick", 
			"muppet",
			"hoodie",
			"notebook",
			"finger-skate",
			"flask",
			"backpack",
			"handpack",
			"purse"};
	private String description = "description";
	private int quantity = 100;
	@Autowired
	private Dates dates;
	
	/**
	 * 
	 * @param productRepository
	 */
	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	/**
	 * This methods assure the creation of one product of each of the types
	 * (name)
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("CHECK IF THERE ARE ENOUGH PRODUCTS IN THE DATABASE");
		for (int i = 0; i < nameOfProducts.length; i++) {
			Date date;
			try {
				date = dates.getParsedDate(new Date());
				loadProduct(nameOfProducts[i], nameOfProducts[i] + " " + description, date, quantity);
			} catch (ParseException e) {	
				log.error(e);
			}
		}
	}

	private void loadProduct(String name, String description, Date arrival, int quantity) {
		List<Product> products = productRepository.findByName(name);
		if (products.isEmpty()) {
			log.debug("INSERTING " + name);
			Product product = new Product(name, description, arrival, quantity);
			productRepository.save(product);
			log.debug("PRODUCT: " + product + " saved on " + productRepository.toString());
		} else {
			log.debug("STILL SOME " + name + " IN THE DATABASE");
		}
	}
}
