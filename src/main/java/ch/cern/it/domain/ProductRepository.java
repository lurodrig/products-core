package ch.cern.it.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
	
	public List<Product> findByName(String name);
	public List<Product> findByNameStartingWith (String term);
	public List<Product> findByNameLike (String term);
	public List<Product> findByNameContaining (String term);
}
