package ch.cern.it.service;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;

import ch.cern.it.domain.Product;
import ch.cern.it.utils.ValidationPatterns;

@Validated
public interface ProductServices {

	public Product saveOrUpdate (@NotNull Product product);
	public List<Product> findByName(
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String name);
	public List<Product> findByNameStartingWith(
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String term);
	public List<Product> findByNameLike(
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String term);
	public List<Product> findByNameContaining(
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String term);
	public List<Product> findAll();
	public Product findById(int id);
	public void delete (int id);
}
