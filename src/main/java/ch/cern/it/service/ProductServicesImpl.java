package ch.cern.it.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.cern.it.domain.Product;
import ch.cern.it.domain.ProductRepository;
import ch.cern.it.utils.StreamsAndCollections;

@Service
public class ProductServicesImpl implements ProductServices {

	private ProductRepository productRepository;
	private StreamsAndCollections<Product> streamsAndCollectionsUtils;

	@Autowired
	public void setStreamsAndCollectionsUtils(StreamsAndCollections<Product> streamsAndCollectionsUtils) {
		this.streamsAndCollectionsUtils = streamsAndCollectionsUtils;
	}

	@Autowired
	public void setProductRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public Product saveOrUpdate(Product product) {
		return productRepository.save(product);
	}

	@Override
	public List<Product> findByName(String name) {
		return productRepository.findByName(name);
	}

	@Override
	public List<Product> findAll() {
		return streamsAndCollectionsUtils.iterableToList(productRepository.findAll());
	}

	@Override
	public void delete(int id) {
		productRepository.delete(id);
	}

	@Override
	public Product findById(int id) {
		return productRepository.findOne(id);
	}

	@Override
	public List<Product> findByNameStartingWith(String term) {
		return productRepository.findByNameStartingWith(term);
	}

	@Override
	public List<Product> findByNameLike(String term) {
		return productRepository.findByNameLike(term);
	}

	@Override
	public List<Product> findByNameContaining(String term) {
		return productRepository.findByNameContaining(term);
	}

}
