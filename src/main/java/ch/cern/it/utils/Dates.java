package ch.cern.it.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Dates {
	
	@Value("${date-format}")
	private String jacksonDateFormat = "yyyy-MM-dd";
	
	public Date getParsedDate(Date date) throws ParseException{
		DateFormat format = new SimpleDateFormat(jacksonDateFormat);
		String formattedDate = format.format(date);
		Date parsedDate = format.parse(formattedDate);
		return parsedDate;
	}
}
