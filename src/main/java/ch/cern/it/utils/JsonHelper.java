package ch.cern.it.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author lurodrig
 *
 */
@Component
public class JsonHelper {
	
	@Value("${date-format}")
	private String jacksonDateFormat;
	
	/**
	 * 
	 * @param jsonString
	 * @param c
	 * @return
	 * @throws Exception
	 */
	public Object deserializeJson(String jsonString, Class c) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		DateFormat dateFormat = new SimpleDateFormat(jacksonDateFormat);
		objectMapper.setDateFormat(dateFormat);
		return objectMapper.readValue(jsonString, c);
	}

}
