package ch.cern.it.utils;

import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.stereotype.Component;

/**
 * 
 * @author lurodrig
 *
 * @param <T>
 */
@Component
public class StreamsAndCollections<T> {

	/**
	 * 
	 * @param iterable
	 * @return
	 */
	public List<T> iterableToList(Iterable<T> iterable) {
		List<T> list = StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(iterable.iterator(), Spliterator.ORDERED), false)
				.collect(Collectors.<T>toList());
		return list;
	}

}
