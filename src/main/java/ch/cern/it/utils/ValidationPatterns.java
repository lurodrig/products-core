package ch.cern.it.utils;

public class ValidationPatterns {
	
	public final static String NAME = "^[a-zA-Z0-9_\\-]*$";
	public final static String DESCRIPTION = "^[a-zA-Z0-9_,.\\-!?@\\s]*$";
}
