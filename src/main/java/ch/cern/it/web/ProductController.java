package ch.cern.it.web;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.cern.it.domain.Product;
import ch.cern.it.service.ProductServices;
import ch.cern.it.utils.ValidationPatterns;

@RestController
public class ProductController {

	@Autowired
	private ProductServices productServices;

	@GetMapping("/products")
	public List<Product> findAll() {
		return productServices.findAll();
	}
	
	@GetMapping("/products/{id}")
	public Product findById(@PathVariable int id) {
		return productServices.findById(id);
	}
	
	@PostMapping("/products")
	public Product insert(@RequestBody @Validated Product product) {
		return productServices.saveOrUpdate(product);
	}
	
	@PutMapping("/products/{id}")
	public Product update(@PathVariable int id, @RequestBody @Validated Product product) {
		product.setId(id);
		return productServices.saveOrUpdate(product);
	}

	@GetMapping("/products/")
	public List<Product> findByName(
			@RequestParam
			@Validated
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String name) {
		return productServices.findByName(name);
	}
	
	@GetMapping("/products/search")
	public List<Product> findByNameContaining(
			@RequestParam
			@Validated
			@Pattern(regexp=ValidationPatterns.NAME) 
			@NotNull 
			@NotEmpty 
			@Size(max=50) String term) {
		return productServices.findByNameContaining(term);
	}

	@DeleteMapping("/products/{id}")
	public void delete(@PathVariable int id) {
		productServices.delete(id);
	}

}
