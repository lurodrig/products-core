package ch.cern.it.cs.ct;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import ch.cern.it.domain.Product;
import ch.cern.it.utils.Dates;
import ch.cern.it.utils.JsonHelper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServicesIntegrationTests {

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	private JsonHelper jsonHelper;
	
	@Autowired
	private Dates dates;

	@Test
	public void testA_getAllProducts() {
		ResponseEntity<Product[]> response = this.testRestTemplate.getForEntity("/products", Product[].class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(response.getBody().length).isGreaterThanOrEqualTo(0);
	}

	@Test
	public void testB_insertWhenProductIsOK() throws Exception {
		Product product = new Product("name-name", "description@-", dates.getParsedDate(new Date()), 100);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Product> requestEntity = new HttpEntity<Product>(product, headers);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/products", requestEntity, String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Product result = (Product) jsonHelper.deserializeJson(response.getBody(), Product.class);
		assertThat(result).isEqualToComparingOnlyGivenFields(product, "name", "description", "quantity");
	}

	@Test
	public void testC_insertWhenProductIsNull() throws Exception {
		Product product = null;
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	public void testD_insertWhenProductIsEmpty() throws Exception {
		Product product = new Product();
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	public void testE_insertWhenNameIsIncorrect() throws Exception {
		Product product = new Product("name?", "description", new Date(), 100);
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	public void testF_insertWhenDescriptionIsIncorrect() throws Exception {
		Product product = new Product("name", "description#", new Date(), 100);
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	public void testG_insertWhenQuantityIsIncorrect() throws Exception {
		Product product = new Product("name", "description", new Date(), 0);
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	@Test
	public void testH_insertWhenArrivalIsNull() throws Exception {
		Product product = new Product("name", "description", null, 100);
		assertThat(insert(product).getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
	}

	private ResponseEntity<String> insert(Product product) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Product> requestEntity = new HttpEntity<Product>(product, headers);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/products", requestEntity, String.class);
		return response;
	}
}
