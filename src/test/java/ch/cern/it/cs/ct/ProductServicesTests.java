package ch.cern.it.cs.ct;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeastOnce;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import ch.cern.it.domain.Product;
import ch.cern.it.domain.ProductRepository;
import ch.cern.it.service.ProductServices;

/**
 * 
 * @author lurodrig
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductServicesTests {

	@Autowired
	private ProductServices services;
	@MockBean
	private ProductRepository repository;

	@Test
	public void testA_insertWhenProductIsOK() {
		Product product = new Product("name-name", "description@-", new Date(), 100);
		given(repository.save(product)).willReturn(product);
		Product result = services.saveOrUpdate(product);
		assertThat(result).isEqualToComparingOnlyGivenFields(product, "name", "description", "arrival", "quantity");
	}

	@Test(expected = ValidationException.class)
	public void testB_insertWhenProductIsNull() {
		Product product = null;
		services.saveOrUpdate(product);
	}

	@Test(expected = ValidationException.class)
	public void testC_insertWhenProductIsEmpty() {
		Product product = new Product();
		given(repository.save(product)).willThrow(new ValidationException());
		services.saveOrUpdate(product);
	}

	@Test(expected = ValidationException.class)
	public void testD_insertWhenNameIsIncorrect() {
		Product product = new Product("name?", "description", new Date(), 100);
		given(repository.save(product)).willThrow(new ValidationException());
		services.saveOrUpdate(product);
	}

	@Test(expected = ValidationException.class)
	public void testE_insertWhenDescriptionIsIncorrect() {
		Product product = new Product("name", "description?", new Date(), 100);
		given(repository.save(product)).willThrow(new ValidationException());
		services.saveOrUpdate(product);
	}

	@Test(expected = ValidationException.class)
	public void testF_insertWhenQuantityIsIncorrect() {
		Product product = new Product("name", "description", new Date(), 0);
		given(repository.save(product)).willThrow(new ValidationException());
		services.saveOrUpdate(product);
	}

	@Test(expected = ValidationException.class)
	public void testG_insertWhenArrivalIsNull() {
		Product product = new Product("name", "description", null, 100);
		given(repository.save(product)).willThrow(new ValidationException());
		services.saveOrUpdate(product);
	}

	@Test
	public void testH_findByNameWhenNameExists() {
		String name = "name";
		Product product = new Product("name", "description", null, 100);
		List<Product> products = new ArrayList<Product>();
		products.add(product);
		given(repository.findByName(name)).willReturn(products);
		List<Product> result = services.findByName(name);
		assertThat(result).containsAll(products);
	}

	@Test(expected = ValidationException.class)
	public void testI_findByNameWhenNameIsNull() {
		String name = null;
		services.findByName(name);
	}

	@Test(expected = ValidationException.class)
	public void testJ_findByNameWhenNameIsIncorrect() {
		String name = "name?";
		services.findByName(name);
	}

	@Test(expected = ValidationException.class)
	public void testK_findByNameWhenNameIsEmpty() {
		String name = "";
		services.findByName(name);
	}

	@Test
	public void testL_findByNameWhenNameNotExists() {
		String name = "xxxx";
		given(repository.findByName(name)).willReturn(new ArrayList<Product>());
		List<Product> result = services.findByName(name);
		assertThat(result).isNullOrEmpty();
	}

	@Test(expected = EmptyResultDataAccessException.class)
	public void testM_deleteWhenIdNotExists() {
		int id = 123456;
		doThrow(EmptyResultDataAccessException.class).when(repository).delete(id);
		services.delete(id);
		verify(repository, atLeastOnce()).delete(id);
	}
}
